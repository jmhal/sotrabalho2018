# Trabalho de Sistemas Operacionais - 2018.2

Este documento descreve o trabalho de Sistemas Operacionais 2018.2, do curso de Redes de Computadores, Campus de Quixadá da UFC. Independente
do conteúdo do trabalho, cada aluno já deve realizar as seguintes tarefas:

1. Você deve criar uma conta do BitBucket e nela estabelecer um repositório chamado **trabalhoso** 
2. Todo código e documentação produzido deve ser adicionados a esse repositório. 
3. Esse repositório deve ser privado.
4. Você deve convidar o professor como membro (usuário: jmhal, e-mail: joao.marcelo@ufc.br).

## Objetivo

O objetivo do trabalho é exercitar os conceitos apresentados na disciplina de Sistemas Operacionais. 
Em detalhes, vamos trabalhar com o conceito de _threads_. Para tal, os alunos devem implementar uma versão do **problema do produtor-consumidor**.

## Descrição do Trabalho 

O trabalho consiste na implementação do projeto de programação **Projeto 3 - Problema do Produtor Consumidor**, descrito ao final do capítulo 05 do livro Fundamentos de Sistemas Operacionais 
(no .pdf, a descrição do problema está nas páginas 226 a 230). Apesar do livro mencionar Windows, para este trabalho estou restrigindo ao Linux. Leiam com atenção toda a descrição do problema. 
Em especial, prestem atenção nos parâmetros que devem ser passados ao programa. Por exemplo, a invocação mais simples na linha de comando seria assim:

```
$ ./produtorconsumidor 20 1 1
PRODUTOR 0: Iniciando Execução.
PRODUTOR 0: Dormindo por 6.
CONSUMIDOR 0: Iniciando Execução.
CONSUMIDOR 0: Dormindo por 1.
PRODUTOR 0: produziu 96
PRODUTOR 0: Dormindo por 1.
CONSUMIDOR 0, consumiu 96
CONSUMIDOR 0: Dormindo por 0.
PRODUTOR 0: produziu 71
PRODUTOR 0: Dormindo por 2.
CONSUMIDOR 0, consumiu 71
CONSUMIDOR 0: Dormindo por 3.
PRODUTOR 0: produziu 42
PRODUTOR 0: Dormindo por 0.
PRODUTOR 0: produziu 80
PRODUTOR 0: Dormindo por 7.
CONSUMIDOR 0, consumiu 42
CONSUMIDOR 0: Dormindo por 7.
PRODUTOR 0: produziu 57
PRODUTOR 0: Dormindo por 0.
PRODUTOR 0: produziu 60
PRODUTOR 0: Dormindo por 5.
CONSUMIDOR 0, consumiu 80
CONSUMIDOR 0: Dormindo por 3.
```

Neste cenário, a simulação executará por 20 segundos, com um produtor gerando inteiros e um consumidor lendo. O tamanho do **buffer** é fixo no código, vamos trabalhar com um _buffer_ de tamanho 5.
Observem no exemplo que o consumidor retira os números na ordem que o produtor os inseriu. Na avaliação, os seguintes cenários serão executados:

```
# Vários produtores e 1 consumidor
$ ./produtorconsumidor 100 10 1
# 1 produtor e vários consumidores
$ ./produtorconsumidor 100 1 10
```

Quatro comportamentos são esperados:

1. Os produtores não conseguem produzir mais do que o tamanho do _buffer_ sem que um consumidor obtenha acesso ao mesmo.
2. Os consumidores não conseguem consumir mais do que o tamanho do _buffer_ sem que um produtor obtenha acesso ao mesmo.
3. Um _buffer_ vazio bloqueia os consumidores.
4. Um _buffer_ completo bloqueia os produtores.

Irei também variar os parâmetros para valores aleatórios e também verificar se os _threads_ estão dormindo de forma aleatório.

### Documentação

Além do código fonte, você deve criar um arquivo **README.md** no repositório descrevendo em detalhes:

+ Configuração da máquina na qual foi feita a análise:
    * Versão do Sistema Operacional e Distribuição Linux 
    * Versão dos compiladores/máquinas virtuais
+ Passo-a-passo para instalação:
    * Pacotes a serem instalados 
    * Etapas para compilação do programa, incluindo a sequência de comandos.

O código pode ser organizado da forma que acharem melhor.

## Ambiente de Desenvolvimento

O ambiente para desenvolvimento e execução do projeto deve ser o *Sistema Operacional Linux*. 
Aqueles que não se sentirem a vontade para instalar o Linux podem utilizar o _VirtualBox_ ou qualquer computador dos laboratórios do campus. 
Poderá ser feito nas linguagens **C** ou **Python**. No caso da opção por **Python**, devem ser usadas as estruturas do pacote _multiprocessing_.
Caso a opção seja por **C**, deve ser utilizada a biblioteca _pthreads_. 
 
## Entrega

O prazo de entrega é dia *01/12/2018*. A apresentação individual será combinada com cada aluno que entregar o trabalho. 

## Avaliação 

O trabalho terá valor de 10,0 (dez) pontos. O trabalho é *INDIVIDUAL*. A divisão dos pontos será da seguinte forma:

+ **Criação da Conta no BitBucket.org** (Avaliação Objetiva) (1,0 Ponto)
    * Se o aluno conseguir criar a conta com as permissões corretas e carregar qualquer código. 
    * Qualquer envio de código por outra maneira (e-mail, outros _sites_) será **desconsiderado**.
+ **Implementação da Solução ** (Avaliação Objetiva) (3,0 Pontos)
    * O código deve compilar e executar em um ambiente _Linux_.
+ **Apresentação Individual** (Avaliação Subjetiva) (6,0 pontos)
    * O aluno terá que explicar ao professor o código entregue, em um momento a ser definido.
    * Não basta compilar, a verificação do código deve demonstrar o controle de concorrência e sincronização.
    * Não são necessários _slides_.

